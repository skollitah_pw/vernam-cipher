def build_key_stream(key, length):
    key_len = len(key)
    key_stream = key * (int(length / key_len) + 1)
    return key_stream[0:length]


def encode(message, key):
    return bytes(a ^ b for (a, b) in zip(message, build_key_stream(key, len(message))))


def decode(cipher, key):
    return encode(cipher, key)


def text_to_bytes(text):
    return bytes(text, encoding='utf-8')


def bytes_to_text(bytes):
    return bytes.decode('utf-8')


if __name__ == '__main__':
    try:
        operation = input("operation e - encrypt, d - decrypt: ")

        if (operation == "e"):
            message = text_to_bytes(input('message: '))
            key = text_to_bytes(input('key: '))
            print(f"Encoded bytes: {encode(message, key).hex()}")
        elif (operation == "d"):
            cipher = bytes.fromhex(input('cipher (hex bytes): '))
            key = text_to_bytes(input('key: '))
            print(f"Decoded text: {bytes_to_text(decode(cipher, key))}")
    except Exception as e:
        raise e
