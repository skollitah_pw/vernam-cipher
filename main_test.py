import unittest

import main


class TestModularInverse(unittest.TestCase):

    def test_encode_with_short_key(self):
        message = 'ala ma kota, a kot ma ale'
        key = 'secret'
        encrypted = main.encode(main.text_to_bytes(message), main.text_to_bytes(key))
        decrypted = main.decode(encrypted, main.text_to_bytes(key))
        self.assertEqual(message, main.bytes_to_text(decrypted))

    def test_encode_with_long_key(self):
        message = 'short msg'
        key = 'extra super secret key'
        encrypted = main.encode(main.text_to_bytes(message), main.text_to_bytes(key))
        decrypted = main.decode(encrypted, main.text_to_bytes(key))
        self.assertEqual(message, main.bytes_to_text(decrypted))


if __name__ == '__main__':
    unittest.main()
